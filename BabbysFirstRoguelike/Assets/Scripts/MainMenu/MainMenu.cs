﻿using UnityEngine;

public class MainMenu : MonoBehaviour
{

    [SerializeField]
    private GameObject mainMenuButtons;
    [SerializeField]
    private GameObject backButton;

    public void PlayButtonPressed()
    {
        mainMenuButtons.SetActive(false);
        backButton.SetActive(true);
    }

    public void SettingsButtonPressed()
    {
        mainMenuButtons.SetActive(false);
        backButton.SetActive(true);
    }

    public void ExitButtonPressed()
    {
        Application.Quit();
    }

    public void BackButtonPressed()
    {
        mainMenuButtons.SetActive(true);
        backButton.SetActive(false);
    }
}
