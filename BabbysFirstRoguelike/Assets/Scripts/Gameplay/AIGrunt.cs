﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIGrunt : MonoBehaviour
{
    [SerializeField]
    private float health = 100f;
    [SerializeField]
    private float damage = 10f;
    [SerializeField]
    private float movementSpeed = 10f;
    [SerializeField]
    private Transform patrolStartLocation, patrolEndLocation;

    private float step;
    private bool reachedTarget = false;
    private bool spottedPlayer = false;
    private Vector2 targetLocation;

    // Start is called before the first frame update
    void Start()
    {
        targetLocation = patrolStartLocation.position;   
    }

    // Update is called once per frame
    void Update()
    {
        if (!spottedPlayer)
        {
            DoPatrolStuff();
        }

        if (spottedPlayer)
        {
            DoShootyStuff();
        }

    }

    void DoPatrolStuff()
    {
        step = movementSpeed * Time.deltaTime;

        if ((Vector2)transform.position == targetLocation && transform.position == patrolStartLocation.position)
        {
            targetLocation = patrolEndLocation.position;
        }

        if ((Vector2)transform.position == targetLocation && targetLocation == (Vector2)patrolEndLocation.position)
        {
            targetLocation = patrolStartLocation.position;
        }

        transform.position = Vector2.MoveTowards(transform.position, targetLocation, step);
    }

    void DoShootyStuff()
    {

    }
}
