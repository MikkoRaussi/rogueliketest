﻿using UnityEngine;

public class PlayerControls : MonoBehaviour
{

    [SerializeField]
    private float walkSpeed = 5f;

    [SerializeField]
    private float jumpForce = 10f;

    private Rigidbody2D rb2d;
    private Vector2 movementVector;
    private bool isJumpPressed = false;
    private bool isGrounded = false;
    private bool shouldJump = false;
    private RaycastHit2D leftGroundCheck, centerGroundCheck, rightGroundCheck;
    private BoxCollider2D playerCol;

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        playerCol = GetComponent<BoxCollider2D>();
    }

    private void Update()
    {
        movementVector.x = Input.GetAxis("Horizontal") * walkSpeed;

        isJumpPressed = Input.GetButtonDown("Jump");
        if (isJumpPressed)
        {
            shouldJump = true;
        }

        leftGroundCheck = Physics2D.Raycast(playerCol.bounds.min, -Vector2.up, 0.1f);
        centerGroundCheck = Physics2D.Raycast(new Vector2(playerCol.bounds.center.x, playerCol.bounds.min.y), -Vector2.up, 0.1f);
        rightGroundCheck = Physics2D.Raycast(new Vector2(playerCol.bounds.max.x, playerCol.bounds.min.y), -Vector2.up, 0.1f);

        if (leftGroundCheck.collider != null || centerGroundCheck.collider != null || rightGroundCheck.collider != null)
        {
            isGrounded = true;
        }

        if (leftGroundCheck.collider == null && centerGroundCheck.collider == null && rightGroundCheck.collider == null)
        {
            isGrounded = false;
        }

    }

    private void FixedUpdate()
    {
        movementVector.y = rb2d.velocity.y;
        rb2d.velocity = movementVector;

        if (shouldJump && isGrounded)
        {
            isGrounded = false;
            shouldJump = false;
            rb2d.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
        }
    }
}
